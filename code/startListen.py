import pyaudio
import wave
import time
from math import log10
import audioop  
import queue
import threading
from tkinter import *


chunk = 1024  # Record in chunks of 1024 samples
sample_format = pyaudio.paInt16  # 16 bits per sample
inChannels = 1
inFS = 16000 # Record at 16000 samples per second

outChannels = 1
outFS = 48000

playFile = "calmBeep.wav"
timeBetweenBeep = 5.0
timeSinceBeep = 0
size = 20
currSize = 0 
currAvg = 0
micDeviceId=2
speakerDeviceId=13
colorThreshold = 0.8 

startTime = time.time()

p = pyaudio.PyAudio()  # Create an interface to PortAudio
threshold = 40.0
done = 1

print('Recording')
#Stores the volume of the past 2 seconds in .1 second intervals.
volQ = queue.Queue(maxsize=size)
stream = p.open(format=sample_format,
                channels=inChannels,
                rate=inFS,
                frames_per_buffer=chunk,
                input=True,
                output=False,
                input_device_index=micDeviceId)

outStream = p.open(format=sample_format,
                channels=outChannels,
                rate=outFS,
                frames_per_buffer=chunk,
                input=False,
                output=True,
                output_device_index=speakerDeviceId)


# Store data in chunks for 3 seconds

def play_sound(out_stream, fileName):
    global done
    global timeSinceBeep
    done = 0
    wf = wave.open(fileName, 'rb')
    data = wf.readframes(chunk)

    # play stream (looping from beginning of file to the end)
    while data:
        # writing to the stream is what *actually* plays the sound.
        out_stream.write(data)
        data = wf.readframes(chunk)
    # cleanup stuff.
    wf.close()
    done = 1
    timeSinceBeep = timeBetweenBeep


window = Tk()
window.geometry("900x500")

beepOn=True
thresholdOn=True


prevColor1 = ""
prevColor2 = ""
disabledColor = "gray"
    
thresholdVar=DoubleVar()
threshold = 40.0
thresholdVar.set(threshold)
timeBetweenVar=DoubleVar()
timeBetweenBeep = 5.0
timeBetweenVar.set(timeBetweenBeep)

window.setvar(name="currAvg", value=currAvg)


def rgb_to_hex(r, g, b):
    #print("Going to do ", r, g, b)
    return '#{:02x}{:02x}{:02x}'.format(r, g, b)
closed = False
def close():
    global closed
    closed = True

closeButton = Button(window, text="Close Program", command=close)
closeButton.place(x=750,y=10)


emptySpace5 = Frame(window,height=40,width=0)
emptySpace5.pack()
moving = Label(text="Current Volume Average: " + str(window.getvar(name="currAvg")), font=('Helvetica bold', 26))
moving.pack()
emptySpace6 = Frame(window,height=20,width=0)
emptySpace6.pack()
timeBetweenFrame = Frame(window)
timeBetweenFrame.pack()

timeBetweenLabel = Label(timeBetweenFrame, text="Time between beeps (in s):", font=('Roman', 17), padx=20)
timeBetweenLabel.pack(side=LEFT)
timeBetweenScale = Scale(timeBetweenFrame, from_=1, to=30,variable=timeBetweenVar,orient=HORIZONTAL,length=200,width=25,resolution=0.5)
timeBetweenScale.pack(side=LEFT)

def switchBeep():
    global beepOn
    global timeBetweenScale
    global prevColor1
    global timeBwButton
    if (beepOn == True):
        beepOn = False
        prevColor1 = (timeBetweenScale.cget("troughcolor"))
        timeBetweenScale.config(state=DISABLED,takefocus=0,troughcolor=disabledColor)
        timeBwButton.config(text="Turn On Beep")
    else:
        beepOn = True
        timeBetweenScale.config(state=NORMAL,takefocus=1,troughcolor=prevColor1)
        timeBwButton.config(text="Turn Off Beep")

def switchThreshold():
    global thresholdOn
    global dbScale
    global prevColor2
    global dbButton
    if (thresholdOn == True):
        thresholdOn = False
        prevColor2 = (dbScale.cget("troughcolor"))
        dbScale.config(state=DISABLED,takefocus=0,troughcolor=disabledColor)
        dbButton.config(text="Turn On Threshold")
        volFrame.config(bg=rgb_to_hex(0,255,0))
    else:
        thresholdOn = True
        
        dbScale.config(state=NORMAL,takefocus=1,troughcolor=prevColor2)        
        dbButton.config(text="Turn Off Threshold")

emptySpace4 = Frame(timeBetweenFrame,height=0,width=20)
emptySpace4.pack(side=LEFT)
timeBwButton = Button(timeBetweenFrame, text="Turn Off Beep", command=switchBeep,pady=10)
timeBwButton.pack(anchor=CENTER)

scaleFrame = Frame(window)
scaleFrame.pack()


dbLabel = Label(scaleFrame, text="Volume Threshold (in dB):", font=('Roman', 17), padx=20)
dbLabel.pack(side=LEFT)
dbScale = Scale(scaleFrame, from_=25, to=100,variable=thresholdVar,orient=HORIZONTAL,length=350,width=25,resolution=0.5)
dbScale.pack(side=LEFT)


emptySpace3 = Frame(scaleFrame,height=0,width=20)
emptySpace3.pack(side=LEFT)
evaluationFrame = LabelFrame(scaleFrame, height=50, width=100,padx=20)
evaluationFrame.pack(side=LEFT, fill=X,expand=33)


emptySpace7 = Frame(window,height=10,width=0)
emptySpace7.pack()
dbButton = Button(window, text="Turn Off Threshold", command=switchThreshold,pady=10)
dbButton.pack()
emptySpace8 = Frame(window,height=10,width=0)
emptySpace8.pack()

evalText = Label(evaluationFrame, text="Noise Level:", font=('Mesquite Std', 9))
evalText.pack()
evalLabel = Label(evaluationFrame, text="Whispering", font=('Mesquite Std', 12))
evalLabel.pack()



emptySpace1 = Frame(window,height=25,width=0)
emptySpace1.pack()
legendFrame = LabelFrame(window, height=50, width=250)
legendFrame.pack()
gvl = Label(legendFrame, text="Good volume: ", font=('Roman ', 14), padx=10)
gvl.pack(side=LEFT)
greenFrame = LabelFrame(legendFrame, height=20, width=20, bg=rgb_to_hex(0,255,0), padx=10)
greenFrame.pack(side=LEFT)
yvl = Label(legendFrame, text="Getting louder: ", font=('Roman ', 14), padx=10)
yvl.pack(side=LEFT)
yellowFrame = LabelFrame(legendFrame, height=20, width=20, bg="yellow", padx=10)
yellowFrame.pack(side=LEFT)
rvl = Label(legendFrame, text="Too loud: ", font=('Roman ', 14), padx=10)
rvl.pack(side=LEFT)
redFrame = LabelFrame(legendFrame, height=20, width=20, bg=rgb_to_hex(255,0,0), padx=10)
redFrame.pack(side=LEFT)

emptySpace2 = Frame(window,height=25,width=0)
emptySpace2.pack()

volFrame = LabelFrame(window, height=100,width=100,bg="red")
volFrame.pack()

emptySpace9 = Frame(window, height=50,width=0)
emptySpace9.pack()







currTot = 0
lastTime = time.time()
while (True):
    if (closed == True):
        break
    data = stream.read(chunk,exception_on_overflow=False)
    rms = audioop.rms(data,2)
    if (rms <= 0): 
        db == 0
    else:
        db = 20 * log10(rms)
    if (volQ.full()):
        currTot -= volQ.get()
        currSize -= 1
    currTot += db
    currSize += 1
    volQ.put(db)
    currAvg = currTot / currSize
    moving.config(text=("Current Volume Average: " + str(round(currAvg, 3))))
    #print("Avg over past 2 seconds: " +  str(currAvg) + "dB")
    if (threshold != thresholdVar.get()):
        threshold = thresholdVar.get()
        if (threshold >= 25 and threshold < 45):
            noiseLevel = "Whispering"
        elif (threshold >= 45 and threshold < 60):
            noiseLevel = "Talking"
        elif (threshold >= 60 and threshold < 75):
            noiseLevel = "Yelling"
        elif (threshold >= 75 and threshold <= 100):
            noiseLevel = "Partying"
        evalLabel.config(text=noiseLevel)
    timeBetweenBeep = timeBetweenVar.get()
    if (timeSinceBeep > timeBetweenBeep):
        timeSinceBeep = timeBetweenBeep
    if (timeSinceBeep > 0):
        timeSinceBeep -= (time.time() - lastTime)
        #print(str(time.time()) + ": Time since beep: " + str(timeSinceBeep))
    lastTime = time.time()

    if (thresholdOn == True):
        maxGood = threshold * colorThreshold
        diff = (threshold - maxGood) / 2

        colorDetermine = max(0, threshold - currAvg)
        #print(colorDetermine)
        #print(diff * 2)
        fungo = -1
        if (colorDetermine > diff): 
            fungo = (1-((colorDetermine-diff)/diff))
        else:
            fungo = colorDetermine / diff
        fungo = max(0, round(fungo * 255))
        #print(fungo)
        if colorDetermine < diff:
            color = [255, fungo, 0]
        elif colorDetermine > diff:

            color = [fungo, 255, 0]
        else:
            color = [255,255,0]

        newColor = rgb_to_hex(color[0], color[1], color[2])
        volFrame.config(bg=newColor)


    if (currAvg > threshold and thresholdOn == True):
        #print("Over threshold")
        if (done == 1):
            #print("Is done")
            if (timeSinceBeep <= 0 and beepOn == True):
                print(str(time.time() - startTime) + "s: Playing beep")
                x = threading.Thread(target=play_sound, args=(outStream,playFile))
                x.start()

    window.update()
    time.sleep(0.1)

# Stop and close the stream 
stream.stop_stream()
stream.close()
outStream.stop_stream()
outStream.close()
# Terminate the PortAudio interface
p.terminate()
window.destroy

print('Finished')

# Save the recorded data as a WAV file
