The seeed-voicecard-master folder is used for installing the necessary requirements
for the ReSpeaker 2-Mics Pi HAT. 
https://www.seeedstudio.com/ReSpeaker-2-Mics-Pi-HAT.html
I did not end up using the mic for this into my final project for several reasons, but
primarily because the audio input for this device was greatly inferior to a surround
sound mic (I ended up using Jabra Speak 510). One thing to also note was that
as of 5/6/2023, the most recent 32 bit and 64 bit architecture for Raspberry Pi
are not compatible with the installation of this, whether you use the ./install.sh
or the ./install_arm64.sh script. I had to install Legacy OS 32-bit onto the
Raspberry Pi in order to successfully install everything that has to do with
this device. This makes it possible to set up an audio connection to a bluetooth
device and send data from the Pi to the speaker.

The bluez-alsa folder is to use for setting up a connection to a bluetooth speaker or mic
and be able to output audio or read input audio in conjunction with pyaudio. 
https://github.com/arkq/bluez-alsa
What I used to setup bluez-alsa was following alongside the commands.sh bash script 
that I got from:
https://gist.github.com/fagnercarvalho/2755eaa492a8aa27081e0e0fe7780d14
If you try to use the bash script, though, make sure you change the device settings to your 
desired bluetooth device. 

The code folder is what I wrote to setup the interface and calculate noise threshold and play
the beep using pyAudio. Use 'python3 startListen.py' to create the interface, and use 'python3
listDevices.py' to list the devices that are comptaible with PyAudio and list their variables. 
