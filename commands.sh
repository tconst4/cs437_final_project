# Install bluealsa to create interface to Bluetooth device

git clone https://github.com/Arkq/bluez-alsa.git
cd bluez-alsa
su
apt-get install libglib2.0-dev -y
apt-get install -y libasound2-dev
apt install -y build-essential autoconf
apt-get install -y libbluetooth-dev
apt-get install libtool -y
apt install libsbc-dev -y
apt-get install build-essential libdbus-glib-1-dev libgirepository1.0-dev
autoreconf --install
mkdir build && cd build
../configure --enable-ofono --enable-debug
make && make install
# Pair, trust and then connect to Bluetooth device

bluetoothctl
power on
agent on
scan on
# Choose the right device address from the list
pair 8C:DE:52:2B:7C:A7
trust 8C:DE:52:2B:7C:A7
info 8C:DE:52:2B:7C:A7
connect 8C:DE:52:2B:7C:A7


# Create file for bluealsa interface

nano ~/.asoundrc

# Add to new file

defaults.bluealsa.interface "org.bluealsa"
defaults.bluealsa.device "8C:DE:52:2B:7C:A7" # change this to your Bluetooth device address
defaults.bluealsa.profile "canz"
defaults.bluealsa.delay 10000


# Download track for testing purposes

wget "http://soundbible.com/grab.php?id=2206&type=wav" -O example.wav --no-check-certificate


# Play track by using aplay. aplay is a .wav file player

aplay -D bluealsa:DEV=8C:DE:52:2B:7C:A7,PROFILE=a2dp example.wav


# In case you have issues when trying to play the tracks check the logs by using the commands below

journalctl -xeu bluealsa.service
journalctl -xeu bluetooth.service
systemctl status bluetooth

# Also, make sure bluealsa is installed by running

bluealsa --version
